import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import Todo from './components/Todo'

let todoList = [
  {
    id: 1,
    title: "Stray little devil",
    completed: true
  },
  {
    id: 2,
    title: "Bleach",
    completed: true
  },
  {
    id: 3,
    title: "naruto",
    completed: false
  }
]

// tâche : 22

function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App">

      <ul>
        {
          todoList.map((todo) => (
            <>
              <li>id : {todo.id}</li>
              <li>titre : {todo.title}</li>
              <li>{`complet : ${todo.completed}`}</li>
              <br />
            </>
          ))
        }
      </ul>


    </div>

  )
}

export default App
