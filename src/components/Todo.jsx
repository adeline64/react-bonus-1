function Todo(props) {

    console.log(props);

    let title = props.title;

    let completed = props.completed;

    return (
      <div className="Todo">
        <p>{title}</p>
        <p> completed {completed ? 'Done' : 'Todo'}.</p>
      </div>
    );
}

export default Todo